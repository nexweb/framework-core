# README #

## Introduction ##

This is the documentation for the Core module of Nexweb Framework. As much as possible, all the projects should import their core functions from this package.

Core functions are:

- PDO instance manager
- Memcache instance manager
- Application Configuration

Along with 2 utility classes:

- Debug to handle IP based restrictions
- DataHolder, a typed data container used to pass data between controllers / viewers

There is still some functionalities that need to be implemented. Please refer to the TODO List below.

### Release Notes ###

See [JIRA Realeases](https://nexweb.atlassian.net/projects/FRAME?selectedItem=com.atlassian.jira.jira-projects-plugin%3Arelease-page&status=all)


## Installation ##

- [Install Composer](https://getcomposer.org/doc/00-intro.md#globally)
    
- Add this git repository to your *composer.json* file
    
```
#!json

{
  "repositories": [
    {
      "type": "vcs",
      "url": "git@bitbucket.org:nexweb/framework-core.git"
    }
  ]
}

```
    
- Add the package to the requirement list
    
```
#!sh

composer require nexweb/framework-core
```

- (Optional) If you are going to use the development branch (master) instead of a stable release, you'll need to modify your *composer.json*:


```
#!json
{
  "minimum-stability": "dev"
}
```


## Usage ##

First, update your bootstrap script for include composer packages. Add this to your index.php, bootstrap.php, app.php, ...:


```
#!php

require __DIR__ . '/../vendor/autoload.php';
```

### Configuration ###

First a few things to note. For Database and Memcache configuration, the first element is considered the default one. Memcache supports 2 drivers: Memcache and Memcached.

There are 2 possible configuration formats:

- YAML, which his easier to read, but has an overhead for parsing the file

```
#!yaml

database:
  R:
    host: my-ro.db.hostname
    port: 3306
    user: my_username
    password: secret_password
    dbname: database_name
  W:
    host: my.db.hostname
    port: 3306
    user: my_username
    password: secret_password
    dbname: database_name

memcache:
  content:
    driver: Memcache
    default_ttl: 86400
    key_prefix: key_prefix
    servers:
      - {host: 127.0.0.1, port: 8016}
      - {host: 127.0.0.1, port: 8017}
```

- PHP array

```
#!php

// Load config
$config = [
        'database' => [
            'R' => [
                'host' => 'my-ro.db.hostname',
                'port' => 3306,
                'user' => 'my_username',
                'password' => 'secret_password',
                'dbname' => 'database_name'
            ],
            'RW' => [
                'host' => 'my.db.hostname',
                'port' => 3306,
                'user' => 'my_username',
                'password' => 'secret_password',
                'dbname' => 'database_name'
            ]
        ],
        'memcache' => [
            'default' => [
                'driver' => 'Memcache',
                'default_ttl' => 86400,
                'key_prefix' => 'buzzy.link',
                'servers' => [
                    ['host' => '127.0.0.1', 'port' => 8016],
                    ['host' => '127.0.0.1', 'port' => 8017]
                ]
            ]
        ]
    ]
);

```

In your bootstrap script or main application class, import the Config namespace and load the configuration file into our Config class:


```
#!php

// Import namespace
use Nexweb\Core\Config\Config;

// Load Configuration file
Config::loadYaml(__DIR__ . '/../application/configuration/master.yaml');
```

### Database ###

Access a database instance using our classic Db class:

```
#!php

// Import namespace
use Nexweb\Core\Database\Db;

// Do some of our database stuff
Db::getIstance('R')->select(...);
```

### Cache ###

Cache is a Memcache wrapper, built to work with one or more Memcache servers.

The list of hosts ensures distribution of the data across multiple servers. If replication is needed, it must be done using a load balancer and a repcache. Make sure at least 1 memcache is setup in the configuration file, then basic operations are made using a singleton instance.

```
#!php

// Import namespace
use Nexweb\Core\Cache\Cache;

// Set / fetch data from cache
Cache::getInstance()->set('myKey', 'myValue');
Cache::getInstance()->get('myKey');
```

### Debug ###

Debug should be used for debugging purpose. It first needs a list of authorized IPs, then debugging code can be executed conditionally.

```
#!php
Debug::allowIps(array('111.111.111.111', '111.111.111.112'));

if (Debug::isIpAllowed()) {
    echo "Information that outside world should never see."
}
```

It also happens to have a very convenient method to find the client's real IP address:

```
#!php
Debug::getClientIp();

```

getClientIp() will return $_SERVER['REMOTE_ADDR'], unless 'trusted_proxies' is set in the application Configuration. In the latter case, it will return the last IP in X-Forwarded-For.

If the application is behind a varnish server with IPs (206.125.164.0/24), then we must add this to the configuration:

```
#!php

// Load config
$config = [
        'database' => [
            ...
        ],
        'memcache' => [
            ...
        ]
        // Possible values
        'trusted_proxies' => ['206.125.164.0/24'],
        'trusted_proxies' => ['206.125.164.132', '206.125.164.134'],
    ]
);

```

### DataHolder ###

DataHolder is used to transfer data from the controllers to the views. It is used to make sure the variables have the right type, and will return and standard Exception if the type doesn't match.

```
#!php
DataHolder::getInstance()->addArray('pins', array($pin1, $pin2, $pin3));
DataHolder::getInstance()->addBoolean('success', true);
DataHolder::getInstance()->addInteger('pinId', 12345);
DataHolder::getInstance()->addObject('user', $loggedInUser);
DataHolder::getInstance()->addString('error_message', 'Login failed');
```

Then retrieving the variables for the views is usually handled by the parent controller, or BaseController:

```
#!php
$vars = DataHolder::getInstance()->getData();
foreach($vars as $key => $value){
    $$key = $value;
}
```

### Exceptions ###

Our framework provides standard exceptions can we should use in our projects, instead of redefining new ones:

- NotFoundException: throw it when a the requested page is not found, this should typically be used to send a 404 error
- AccessDeniedException: throw it when a resource cannot be access due to missing permissions (ex: user should be logged in to view this page), this should be used to send a 403 error

## TODO List ##

### Create a DbCache (or something like that) class ##

We initially wrote this framework for iSexy, one of our biggest mistake while it was that the static classes DbFetch* only use de default database and the default cache. It is not possible to use 2 different connections with memcache support.

### Support LOCAL / VM environnement ###

We always use custom code to check if we are working inside a LOCAL or VM setup. This should be moved to the Debug class.

### JSON Config ###

Add support for JSON configuration files -> Config::loadJson()

## Contribute ##

* Create new methods
* Write tests
* Review Code