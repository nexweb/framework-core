<?php
use Nexweb\Core\Config\Config;

class ConfigTest extends \PHPUnit\Framework\TestCase
{

    protected function setUp()
    {
        // Reset static array to its original state
        Config::reset();
    }

    public function testLoadYaml() {

        $yamlTestFile = __DIR__ . "/test.yml";

        // Create a test yaml file
        file_put_contents($yamlTestFile, "Yaml:
    key1: value_1
    key2: value_2
    array1:
        key3: value_3
");
        Config::loadYaml($yamlTestFile);

        unlink($yamlTestFile);

        $this->assertArrayHasKey('key1', Config::get('Yaml'));
        $this->assertArrayHasKey('key2', Config::get('Yaml'));
        $this->assertEquals('value_1', Config::get('Yaml.key1'));
        $this->assertEquals('value_2', Config::get('Yaml.key2'));
        $this->assertArrayHasKey('key3', Config::get('Yaml.array1'));
        $this->assertEquals('value_3', Config::get('Yaml.array1.key3'));
    }

    public function testLoadMultiFilesAppend()
    {

        // Create a first test yaml file
        $yamlTestFile1 = __DIR__ . "/test1.yml";
        file_put_contents($yamlTestFile1, "Yaml:
    key1: value_1
");

        // Create a second test yaml file
        $yamlTestFile2 = __DIR__ . "/test2.yml";
        file_put_contents($yamlTestFile2, "Yaml:
    key2: value_2
");

        Config::loadYaml($yamlTestFile1, true);
        Config::loadYaml($yamlTestFile2, true);

        $this->assertEquals('value_1', Config::get('Yaml.key1'));
        $this->assertEquals('value_2', Config::get('Yaml.key2'));

        // Delete files
        unlink($yamlTestFile1);
        unlink($yamlTestFile2);
    }

    public function testLoadMultiFilesReplace()
    {

        // Create a first test yaml file
        $yamlTestFile1 = __DIR__ . "/test1.yml";
        file_put_contents($yamlTestFile1, "Yaml:
    key1: value_1
");

        // Create a second test yaml file
        $yamlTestFile2 = __DIR__ . "/test2.yml";
        file_put_contents($yamlTestFile2, "Yaml:
    key2: value_2
");
        Config::loadYaml($yamlTestFile1, true);
        Config::loadYaml($yamlTestFile2, false);

        $this->assertArrayNotHasKey('key1', Config::get('Yaml'));
        $this->assertEquals('value_2', Config::get('Yaml.key2'));

        // Delete files
        unlink($yamlTestFile1);
        unlink($yamlTestFile2);
    }

    public function testLoadArray()
    {

        // Create a test config array
        $config = array(
            "Yaml" => array(
                "key1" => "value_1",
                "key2" => "value_2",
                "array1" => array("key3" => "value_3")
            )
        );

        Config::loadArray($config);

        $this->assertArrayHasKey('key1', Config::get('Yaml'));
        $this->assertArrayHasKey('key2', Config::get('Yaml'));
        $this->assertEquals('value_1', Config::get('Yaml.key1'));
        $this->assertEquals('value_2', Config::get('Yaml.key2'));
        $this->assertArrayHasKey('key3', Config::get('Yaml.array1'));
        $this->assertEquals('value_3', Config::get('Yaml.array1.key3'));
    }

    public function testLoad()
    {
        $yamlTestFile = __DIR__ . "/test.yml";

        // Create a test yaml file
        file_put_contents($yamlTestFile, "Yaml:
    key1: value_1
    key2: value_2
    array1:
        key3: value_3
");
        Config::load($yamlTestFile);

        unlink($yamlTestFile);

        $this->assertArrayHasKey('key1', Config::get('Yaml'));
        $this->assertArrayHasKey('key2', Config::get('Yaml'));
        $this->assertEquals('value_1', Config::get('Yaml.key1'));
        $this->assertEquals('value_2', Config::get('Yaml.key2'));
        $this->assertArrayHasKey('key3', Config::get('Yaml.array1'));
        $this->assertEquals('value_3', Config::get('Yaml.array1.key3'));
    }

    public function testReset()
    {
        // Create a test config array
        $config = array(
            "Yaml" => array(
                "key1" => "value_1",
                "key2" => "value_2",
                "array1" => array("key3" => "value_3")
            )
        );

        Config::loadArray($config);
        Config::reset();

        $this->assertCount(0, Config::getAll());
    }

    public function testGetAll()
    {
        // Create a test config array
        $config = array(
            "Yaml" => array(
                "key1" => "value_1",
                "key2" => "value_2",
                "array1" => array("key3" => "value_3")
            )
        );

        Config::loadArray($config);

        $this->assertEquals($config, Config::getAll());
    }

    public function testGet()
    {
        // Create a test config array
        $config = array(
            "key1" => "value_1",
            "key2" => "value_2",
            "array1" => array("key3" => "value_3")
        );

        Config::loadArray($config);

        $this->assertEquals('value_1', Config::get('key1'));
        $this->assertEquals('value_2', Config::get('key2'));
    }
}
