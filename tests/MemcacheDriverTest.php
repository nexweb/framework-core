<?php
use Nexweb\Core\Config\Config;

class MemcacheDriverTest extends \PHPUnit\Framework\TestCase
{

    public $config = [
        'driver' => 'Memcache',
        'default_ttl' => 86400,
        'key_prefix' => 'test',
        'servers' => [
            ['host' => '127.0.0.1', 'port' => 11211]
        ]
    ];

    protected function setUp()
    {
        // Reset static array to its original state
        Config::reset();
    }

    public function testConnect()
    {
        // Memcache doesn't support php 7
        if (phpversion() >= 7.0) {
            return;
        }

        $memcache = new \Nexweb\Core\Cache\MemcacheDriver('default', $this->config);
        $res = $memcache->set('test', 'value');
        $this->assertEquals(true, $res);
    }

    public function testGet()
    {
        // Memcache doesn't support php 7
        if (phpversion() >= 7.0) {
            return;
        }

        $memcache = new \Nexweb\Core\Cache\MemcacheDriver('default', $this->config);
        $memcache->set('test_get', 'value');
        $this->assertEquals('value', $memcache->get('test_get'));
    }

    public function testIncrement()
    {
        // Memcache doesn't support php 7
        if (phpversion() >= 7.0) {
            return;
        }

        $memcache = new \Nexweb\Core\Cache\MemcacheDriver('default', $this->config);

        $object = new stdClass();
        $object->row = 0;

        $memcache->set('test_increment', $object);
        $memcache->incrementValueInRow('test_increment', 'row', 1);

        $cachedObject = $memcache->get('test_increment');
        $this->assertEquals(1, $cachedObject->row);
    }

    public function testDecrement()
    {
        // Memcache doesn't support php 7
        if (phpversion() >= 7.0) {
            return;
        }

        $memcache = new \Nexweb\Core\Cache\MemcacheDriver('default', $this->config);

        $object = new stdClass();
        $object->row = 4;

        $memcache->set('test_decrement', $object);
        $memcache->decrementValueInRow('test_decrement', 'row', 1);

        $cachedObject = $memcache->get('test_decrement');
        $this->assertEquals(3, $cachedObject->row);
    }

}