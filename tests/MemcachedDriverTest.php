<?php

class MemcachedDriverTest extends \PHPUnit\Framework\TestCase
{

    public $config = [
        'driver' => 'Memcached',
        'default_ttl' => 86400,
        'key_prefix' => 'test',
        'servers' => [
                ['host' => '127.0.0.1', 'port' => 11211]
        ]
    ];

    protected function setUp()
    {
    }

    public function testConnect()
    {
        $memcached = new \Nexweb\Core\Cache\MemcachedDriver('default', $this->config);
        $res = $memcached->set('memcached_test_connect', 'valued');
        $this->assertEquals(true, $res);
    }

    public function testGet()
    {
        $memcached = new \Nexweb\Core\Cache\MemcachedDriver('default', $this->config);
        $memcached->set('memcached_test', 'value');
        $this->assertEquals('value', $memcached->get('memcached_test'));
    }

    public function testGetMulti()
    {
        $memcached = new \Nexweb\Core\Cache\MemcachedDriver('default', $this->config);

        $testValues = [];
        for ($i = 1 ; $i <= 10 ; $i++) {
            $testValues['memcached_multi_' . $i] = 'value_' . $i;
        }

        foreach ($testValues as $testKey => $testValue) {
            $memcached->set($testKey, $testValue);
        }

        $result = $memcached->getMulti(array_keys($testValues));

        $this->assertInternalType('array', $result);

        foreach ($testValues as $testKey => $testValue) {
            $this->assertEquals($testValue, $memcached->get($testKey));
        }
    }

    public function testUnsanitizedKeys()
    {
        $memcached = new \Nexweb\Core\Cache\MemcachedDriver('default', $this->config);
        $memcached->set('unsanitized key 1', 'value1');
        $this->assertEquals('value1', $memcached->get('unsanitized key 1'));
        $memcached->set('unsanitized
key 2', 'value2');
        $this->assertEquals('value2', $memcached->get('unsanitized
key 2'));
    }

    public function testIncrement()
    {
        $memcached = new \Nexweb\Core\Cache\MemcachedDriver('default', $this->config);

        $object = new stdClass();
        $object->row = 0;

        $memcached->set('memcached_increment', $object);
        $memcached->incrementValueInRow('memcached_increment', 'row', 1);

        $cachedObject = $memcached->get('memcached_increment');
        $this->assertEquals(1, $cachedObject->row);
    }

    public function testDecrement()
    {
        $memcached = new \Nexweb\Core\Cache\MemcachedDriver('default', $this->config);

        $object = new stdClass();
        $object->row = 4;

        $memcached->set('memcached_decrement', $object);
        $memcached->decrementValueInRow('memcached_decrement', 'row', 1);

        $cachedObject = $memcached->get('memcached_decrement');
        $this->assertEquals(3, $cachedObject->row);
    }

    public function testGetOrSet()
    {
        $memcached = new \Nexweb\Core\Cache\MemcachedDriver('default', $this->config);

        $callable = function() use (&$counter) {
            return 'my_value';
        };

        $this->assertEquals('my_value', $memcached->getOrSet('memcached_get_or_set', $callable));
        $this->assertEquals('my_value', $memcached->get('memcached_get_or_set'));
    }

}