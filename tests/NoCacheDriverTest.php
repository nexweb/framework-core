<?php
use Nexweb\Core\Config\Config;

class NoCacheDriverTest extends \PHPUnit\Framework\TestCase
{

    protected function setUp()
    {
        // Reset static array to its original state
        Config::reset();
    }

    public function testConstructor()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertNotEmpty($memcache);
    }

    public function testGet()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->get('test_get'));
    }

    public function testSet()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->set('test_get', 'field'));
    }

    public function testUpdateValue()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->updateValueInRow('key', 'field', 'value'));
    }

    public function testIncrement()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->incrementValueInRow('test_get', 'field'));
    }

    public function testDecrement()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->decrementValueInRow('test_get', 'field'));
    }

    public function testDelete()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->delete('key'));
    }

    public function testInsertId()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->insertId('key', 'id'));
    }

    public function testDeleteId()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->deleteId('key', 'id'));
    }

    public function testFlush()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->flush('pool'));
    }

    public function testFlushAll()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->flushAll());
    }

    public function testStats()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->stats());
    }

    public function testMulti()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);
        $this->assertFalse($memcache->getMulti(['key1', 'key2']));
    }

    public function testGetOrSet()
    {
        $memcache = new \Nexweb\Core\Cache\NoCacheDriver('default', []);

        $callable = function() {
            return 'my_value';
        };

        $this->assertEquals('my_value', $memcache->getOrSet('key', $callable));
    }

}