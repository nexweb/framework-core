<?php

use Nexweb\Core\Utilities\Debug;
use Nexweb\Core\Config\Config;

class DebugTest extends \PHPUnit\Framework\TestCase
{

    protected function setUp()
    {
        // Reset static array to its original state
        Debug::reset();
    }

    public function testAllowIp()
    {
        Debug::allowIp('206.123.14.12');
        $this->assertCount(1, Debug::$allowedIps);

        Debug::allowIp('2001:0db8:85a3:0000:0000:8a2e:0370:7334');
        $this->assertCount(2, Debug::$allowedIps);
    }

    public function testAllowIps()
    {

        $this->expectException(RuntimeException::class);

        Debug::allowIps('192.168.0.1');
        $this->assertCount(0, Debug::$allowedIps);

        Debug::allowIp(['206.123.14.12', '206.123.14.13']);
        $this->assertCount(2, Debug::$allowedIps);

        Debug::allowIp(['2001:0db8:85a3:0000:0000:8a2e:0370:7334', '2001:0db8:85a3:0000:0000:8a2e:0370:7335']);
        $this->assertCount(4, Debug::$allowedIps);
    }

    public function testGetClientIpWithoutProxy()
    {

        $ip = '120.120.120.120';
        $_SERVER['REMOTE_ADDR'] = $ip;
        $this->assertEquals($_SERVER['REMOTE_ADDR'], Debug::getClientIp());

        $forwardedFor = '123.123.123.123';
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $forwardedFor;
        $this->assertEquals($_SERVER['REMOTE_ADDR'], Debug::getClientIp());
    }

    public function testGetClientIpWithProxy()
    {

        $ip = '120.120.120.120';
        $ip2 = '120.120.120.121';
        $mask = '120.120.120.0/24';
        $_SERVER['REMOTE_ADDR'] = $ip;

        $forwardedFor = '123.123.123.123';
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $forwardedFor;
        $this->assertEquals($ip, Debug::getClientIp());

        \Nexweb\Core\Config\Config::loadArray([
            'trusted_proxies' => [$ip]
        ]);
        $this->assertEquals($forwardedFor, Debug::getClientIp());

        \Nexweb\Core\Config\Config::reset();
        \Nexweb\Core\Config\Config::loadArray([
            'trusted_proxies' => [$ip . "/32"]
        ]);
        $this->assertEquals($forwardedFor, Debug::getClientIp());

        \Nexweb\Core\Config\Config::reset();
        \Nexweb\Core\Config\Config::loadArray([
            'trusted_proxies' => [$mask]
        ]);
        $this->assertEquals($forwardedFor, Debug::getClientIp());

        \Nexweb\Core\Config\Config::reset();
        \Nexweb\Core\Config\Config::loadArray([
            'trusted_proxies' => [$ip, $ip2]
        ]);
        $this->assertEquals($forwardedFor, Debug::getClientIp());

        \Nexweb\Core\Config\Config::reset();
        \Nexweb\Core\Config\Config::loadArray([
            'trusted_proxies' => ['4.4.4.4']
        ]);
        $this->assertNotEquals($forwardedFor, Debug::getClientIp());
    }

}