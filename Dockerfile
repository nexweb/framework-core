FROM ubuntu

RUN apt-get update

# Install git
RUN apt-get install -y git

# Install PHP
RUN apt-get install -y php
RUN apt-get install -y php-xdebug
RUN apt-get install -y php7.0-mysql
RUN apt-get install -y php-memcache
RUN apt-get install -y php-memcached
RUN apt-get install -y php-zip

# Install phpunit
RUN apt-get install -y phpunit

# Install composer
RUN apt-get install -y wget
RUN wget https://getcomposer.org/installer
RUN php installer --install-dir=bin --filename=composer
RUN rm installer

# Install mysql
RUN echo mysql-server mysql-server/root_password password password | debconf-set-selections;\
  echo mysql-server mysql-server/root_password_again password password | debconf-set-selections;\
  apt-get install -y mysql-server mysql-client libmysqlclient-dev
EXPOSE 3306

# Install memcached
RUN apt-get install -y memcached
EXPOSE 11211

#CMD service memcached start && service mysql start
