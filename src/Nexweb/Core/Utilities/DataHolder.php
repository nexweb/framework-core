<?php

namespace Nexweb\Core\Utilities;

class DataHolder {

	private static $self;

	private $strings    = array();
	private $objects    = array();
	private $arrays     = array();
	private $booleans   = array();
	private $integers   = array();

    /**
     * Create and Get a single DataHolder instance
     *
     * @return DataHolder
     */
	public static function getInstance() {
	
		if (is_null(self::$self)) {
			self::$self = new DataHolder();
		}
		
		return self::$self;
	}

    /**
     * Return the entire data
     *
     * @return array
     */
	public function getData() {

		return array_merge($this->strings, $this->objects, $this->arrays, $this->booleans, $this->integers);
	}

    /**
     * Reset all data arrays
     */
	public function reset() {

		$this->strings  = array();
		$this->objects  = array();
		$this->arrays   = array();
		$this->booleans = array();
		$this->integers = array();
	}

    /**
     * @param string $identifier
     * @param bool $value
     * @throws \Exception
     */
	public function addBoolean($identifier, $value) {
		
		if (!is_bool($value)) {
            throw new \Exception('DATAHOLDER_BOOLEAN ($' . $identifier . ')');
        }
			
		$this->booleans[$identifier] = $value;
	}

    /**
     * @param string $identifier
     * @param string $value
     * @throws \Exception
     */
	public function addString($identifier, $value) {
	
		if (!is_string($value)) {
            throw new \Exception('DATAHOLDER_STRING ($' . $identifier . ')');
        }
			
		$this->strings[$identifier] = $value;
	}

    /**
     * @param string $identifier
     * @param object $value
     * @throws \Exception
     */
	public function addObject($identifier, $value) {
		
		if (!is_object($value)) {
            throw new \Exception('DATAHOLDER_OBJECT ($' . $identifier . ')');
        }
			
		$this->objects[$identifier] = $value;
	}

    /**
     * @param string $identifier
     * @param array $value
     * @throws \Exception
     */
	public function addArray($identifier, $value){
	
		if (!is_array($value)) {
            throw new \Exception('DATAHOLDER_ARRAY ($' . $identifier . ')');
        }
			
		$this->arrays[$identifier] = $value;
	}

    /**
     * @param string $identifier
     * @param int $value
     * @throws \Exception
     */
	public function addInteger($identifier, $value) {

        if (!is_numeric($value)) {
            throw new \Exception('DATAHOLDER_INTEGER ($' . $identifier . ')');
        }

		$this->integers[$identifier] = $value;
	}

    /**
     * @param string $identifier
     * @param bool $default
     * @return bool
     */
	public function getBoolean($identifier, $default = false) {
		return isset($this->booleans[$identifier]) ? $this->booleans[$identifier] : $default;
	}

    /**
     * @param string $identifier
     * @param string $default
     * @return string
     */
	public function getString($identifier, $default = '') {
		return isset($this->strings[$identifier]) ? $this->strings[$identifier] : $default;
	}

    /**
     * @param string $identifier
     * @param array $default
     * @return array
     */
	public function getArray($identifier, $default=array()) {
		return isset($this->arrays[$identifier]) ? $this->arrays[$identifier] : $default;
	}

    /**
     * @param $identifier
     * @param $value
     * @throws \Exception
     */
	public function addToArray($identifier, $value){
	
		if (!isset($this->arrays[$identifier])) {
            throw new \Exception('DATAHOLDER_ARRAY');
        }
			
		array_push($this->arrays[$identifier], $value);
	}
}