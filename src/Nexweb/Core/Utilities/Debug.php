<?php

namespace Nexweb\Core\Utilities;

use Nexweb\Core\Config\Config;
use Symfony\Component\HttpFoundation\IpUtils;

class Debug
{

    public static $allowedIps = array();

    /**
     * Use this method to add an IP to the list
     * of authorized ips.
     *
     * @param string $ip
     *
     */
    public static function allowIp($ip)
    {
        self::$allowedIps[] = $ip;
    }

    /**
     * Use this method to add many IPs to the list
     * of authorized ips.
     *
     * @param string[] $ips
     *
     */
    public static function allowIps($ips)
    {
        if (!is_array($ips)) {
            throw new \RuntimeException('Invalid type provided for $ips');
        }

        foreach ($ips as $ip) {
            self::allowIp($ip);
        }
    }

    /**
     * Use this method in a if() to restrict access
     *
     * @return boolean
     */
    public static function isIpAllowed()
    {
        $clientIp = Debug::getClientIp();
        if (empty($clientIp)) {
            return false;
        }

        foreach (self::$allowedIps as $allowedIp) {
            if (strpos($clientIp, $allowedIp) === 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the IP address of the current client. Supports IPv6 and proxies.
     *
     * @return string
     */
    public static function getClientIp()
    {
        $trustedProxies = Config::get('trusted_proxies');
        if (!$trustedProxies) {
            $trustedProxies = [];
        }

        $clientIp = $_SERVER['REMOTE_ADDR'];

        if (!IpUtils::checkIp($clientIp, $trustedProxies)) {
            return $clientIp;
        }

        $clientIps = array();

        // If it was sent through a reverse proxy (varnish), it will be added in X-Forwarded-For
        // The last one is the closest from our server
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {

            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ips = array_reverse($ips);

            foreach ($ips as $ip) {
                $ip = trim($ip);
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {
                    $clientIps[] = $ip;
                }
            }
        }

        $clientIps[] = $clientIp;
        $clientIp = $clientIps[0];

        // expand ipv6 address and remove leading 0s
        if (filter_var($clientIp, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
            $clientIp = inet_ntop(inet_pton($clientIp));
        }

        return $clientIp;
    }

    public static function reset()
    {
        self::$allowedIps = array();
    }
}
