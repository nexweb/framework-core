<?php

namespace Nexweb\Core\Database;
use Nexweb\Core\Cache\Cache;

/**
 * Wrapper class fetch a single row of data from PDO, with optional memcache
 * @author mimbert
 *
 */
class DbFetchRow
{

	/**
	 * @param string $sql
	 * @param array $parameters
	 * @param string $model
	 * @param string|null $cacheKey
	 * @param int $ttl
	 * @return object|false
	 * @throws \PDOException
	 */
	public static function forQuery($sql, $parameters = array(), $model = 'stdClass', $cacheKey = null, $ttl = 3600)
    {
		// check if class exists
		if (!class_exists($model)) {
			throw new \RuntimeException('Unknown class ' . $model);
		}
		
		// get the value from memcache
		if (!is_null($cacheKey)) {
			$result = Cache::getInstance()->get($cacheKey);
			if ($result !== false) {
				return $result;
			}
		}
		
		// if not found in memcache, try database
		$result = Db::getInstance()->selectRow($sql, $parameters, $model);

		// if cache is enabled and a key was provided, set the value in memcache
		if (!is_null($cacheKey)) {
			Cache::getInstance()->set($cacheKey, $result, $ttl);
		}

		return $result;
	}

}