<?php

namespace Nexweb\Core\Database;
use Nexweb\Core\Cache\Cache;

/**
 * Wrapper class fetch a single scalar value from PDO, with optional memcache
 * @author mimbert
 *
 */
class DbFetchValue
{
	/**
	 * @param string $sql
	 * @param array $parameters
	 * @param string|null $cacheKey
	 * @param int $ttl
	 * @return mixed
	 * @throws \PDOException
	 */
	public static function forQuery($sql, $parameters = array(), $cacheKey = null, $ttl = 3600)
	{
		// if cache is enabled and a key was provided, get the value from memcache
		if (!is_null($cacheKey)) {
			$result = Cache::getInstance()->get($cacheKey);
			if ($result !== false) {
				return $result;
			}
		}
		
		$result = Db::getInstance()->selectValue($sql, $parameters);

		// if cache is enabled and a key was provided, set the value in memcache
		if (!is_null($cacheKey)) {
			Cache::getInstance()->set($cacheKey, $result, $ttl);
		}

		return $result;
	}

}