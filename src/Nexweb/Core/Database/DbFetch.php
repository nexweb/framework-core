<?php

namespace Nexweb\Core\Database;
use Nexweb\Core\Cache\Cache;

/**
 * Wrapper class fetch multiple rows of data from PDO, with optional memcache
 */
class DbFetch {

	/**
	 * @param string $sql
	 * @param array $parameters
	 * @param string $model
	 * @param string|null $cacheKey
	 * @param int $ttl
	 * @return object[]|bool
	 * @throws \PDOException
	 */
	public static function forQuery($sql, $parameters = array(), $model = 'stdClass', $cacheKey = null, $ttl = 3600) {

		// if cache is enabled and a key was provided, get the value from memcache
		if (!is_null($cacheKey)) {
			$result = Cache::getInstance()->get($cacheKey);
			if ($result !== false) {
				return $result;
			}
		}

		// get result from db
		$result = Db::getInstance()->select($sql, $parameters, $model);

		// if cache is enabled and a key was provided, set the value in memcache
		if (!is_null($cacheKey)) {
			Cache::getInstance()->set($cacheKey, $result, $ttl);
		}

		return $result;
	}

    /**
     * @param string $sql
     * @param array $parameters
     * @param $model
     * @param string|null $cacheKey
     * @param int $ttl
     * @return array (
     *      "results" => object[],
     *      "total" => int
     * )
	 * @throws \PDOException
     */
    public static function forLimit($sql, $parameters = array(), $model = 'stdClass', $cacheKey = null, $ttl = 3600)
    {

        // if cache is enabled and a key was provided, get the value from memcache
        if (!is_null($cacheKey)) {
            $resultArray = Cache::getInstance()->get($cacheKey);
            if ($resultArray !== false) {
                return $resultArray;
            }
        }

        $resultArray = array();

        $results = DbFetch::forQuery($sql, $parameters, $model);
        $resultArray["results"] = $results;

        $foundRows = DbFetchValue::forQuery("SELECT FOUND_ROWS() AS total", array());
        $resultArray["total"] = $foundRows;

        // if cache is enabled and a key was provided, set the value in memcache
        if (!is_null($cacheKey)) {
            Cache::getInstance()->set($cacheKey, $resultArray, $ttl);
        }

        return $resultArray;
    }

	/**
	 * Retrieve a list of IDs
	 *
	 * @param string $sql SQL query
	 * @param array $parameters Query parameters
	 * @param string $id Name of ID
	 * @param string $cacheKey Memcache key
	 * @param int|number $ttl Memcache key expiration (in seconds)
	 * @return int[] List of ids
	 * @throws \PDOException
	 */
	public static function idsForQuery($sql, $parameters, $id, $cacheKey = null, $ttl = 3600) {

		// if cache is enabled and a key was provided, get the value from memcache
		if (!is_null($cacheKey)) {
			$result = Cache::getInstance()->get($cacheKey);
			if ($result !== false) {
				return $result;
			}
		}

		// fetch results from database
		$result = Db::getInstance()->select($sql, $parameters, 'stdClass');

		// build a list of ids
		$ids = array();
		foreach ($result as $r) {
			$ids[] = $r->$id;
		}

		// if cache is enabled and a key was provided, set the value in memcache
		if (!is_null($cacheKey)) {
			Cache::getInstance()->set($cacheKey, $ids, $ttl);
		}

		return $ids;
	}

	/**
	 * Retrieve a list of paginated IDs
	 *
	 * @param string $sql SQL query
	 * @param array $parameters Query parameters
	 * @param string $id Name of ID
	 * @param int $limit Query limit
	 * @param int $offset Query offset
	 * @param string $cacheKey Memcache key
	 * @param int $ttl Memcache key expiration (in seconds)
	 * @return int[] List of ids
	 * @throws \PDOException
	 */
	public static function idsForQueryLimit($sql, $parameters, $id, $limit, $offset, $cacheKey = null, $ttl = 86400)
	{

		// get all ids for the query
		$ids = DbFetch::idsForQuery($sql, $parameters, $id, $cacheKey, $ttl);

		// extract the part
		$results = array_slice($ids, $offset, $limit);
		$total = count($ids);

		// build and return the result array
		return array("results" => $results, "total" => $total);
	}
}
