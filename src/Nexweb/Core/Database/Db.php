<?php

namespace Nexweb\Core\Database;

use Nexweb\Core\Config\Config;

class Db
{

    private static $instances;
    private $db;

    /**
     * Db constructor.
     * @param string $dbId (optional) Database ID from the configuration file, will pick the first one if omitted
     * @throws \RuntimeException
     * @throws \PDOException
     */
    private function __construct($dbId = null)
    {
        $dbs = Config::get('database');

        if (!$dbs) {
            throw new \RuntimeException('No database found in configuration');
        }

        // If not database ID was passed, let's grab the first one as the default
        if (is_null($dbId)) {
            $dbIds = array_keys($dbs);
            $dbId = $dbIds[0];
        }

        if (!isset($dbs[$dbId])) {
            throw new \RuntimeException('UNKNOWN_DATABASE');
        }

        $host = $dbs[$dbId]['host'];
        $port = $dbs[$dbId]['port'];
        $dbname = $dbs[$dbId]['dbname'];
        $user = $dbs[$dbId]['user'];
        $password = $dbs[$dbId]['password'];

        $this->db = new \PDO("mysql:host={$host};port={$port};dbname={$dbname};charset=utf8", $user, $password);
        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @param string $mode
     * @return Db
     */
    public static function getInstance($mode = null)
    {
        if (!isset(self::$instances[$mode])) {
            self::$instances[$mode] = new Db($mode);
        }

        return self::$instances[$mode];

    }

    /**
     * Return an instance of the PDO connection, in case one needs to use it directly
     *
     * @return \PDO
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     *
     * @param string $sql SQL query
     * @param array $parameters Query parameters
     * @return string|bool
     * @throws \PDOException
     * @throws \RuntimeException
     */
    public function selectValue($sql, $parameters = array())
    {
        $statement = $this->db->prepare($sql);
        $statement->setFetchMode(\PDO::FETCH_NUM);

        $statement->execute($parameters);

        $row = $statement->fetch();
        if (!$row) {
            return false;
        }

        // there should be only 1 key
        $keys = array_keys($row);
        if (count($keys) != 1) {
            throw new \RuntimeException("INVALID_QUERY");
        }

        if (isset($row[$keys[0]])) {
            return $row[$keys[0]];
        } else {
            return false;
        }
    }

    /**
     *
     * @param string $sql SQL query
     * @param array $parameters Query parameters
     * @param string $class
     * @return object|bool
     * @throws \PDOException
     */
    public function selectRow($sql, $parameters = array(), $class = 'stdClass')
    {
        $res = $this->select($sql, $parameters, $class);

        if (isset($res[0])) {
            return $res[0];
        } else {
            return false;
        }

    }

    /**
     *
     * @param string $sql SQL query
     * @param array $parameters Query parameters
     * @param string $class
     * @return object[]|bool
     * @throws \PDOException
     */
    public function select($sql, $parameters = array(), $class = 'stdClass')
    {
        $statement = $this->db->prepare($sql);
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $statement->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $class);

        $statement->execute($parameters);

        return $statement->fetchAll();
    }

    /**
     * @param string $sql
     * @param array $parameters
     * @return string
     * @throws \PDOException
     */
    public function insert($sql, $parameters = array())
    {
        $statement = $this->db->prepare($sql);
        $statement->execute($parameters);

        return $this->db->lastInsertId();
    }

    /**
     * @param string $sql
     * @param array $parameters
     * @return int
     * @throws \PDOException
     */
    public function update($sql, $parameters = array())
    {
        $statement = $this->db->prepare($sql);
        $statement->execute($parameters);

        return $statement->rowCount();
    }

    /**
     * @param $sql
     * @param array $parameters
     * @return int
     * @throws \PDOException
     */
    public function delete($sql, $parameters = array())
    {
        $statement = $this->db->prepare($sql);
        $statement->execute($parameters);

        return $statement->rowCount();
    }
}