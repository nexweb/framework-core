<?php

namespace Nexweb\Core\Config;

use Symfony\Component\Yaml\Parser;

/**
 * Class Config
 * @package Nexweb\Core\Config
 * @link https://bitbucket.org/nexweb/framework-core
 */
class Config {

    private static $config = array();

    /**
     * @deprecated
     * Config::load() is just an alias for Config::loadYaml()
     *
     * @param string $file
     * @return bool
     */
    public static function load($file)
    {
        return self::loadYaml($file);
    }

    /**
     * Load a Yaml file to application configuration
     *
     * @param string $file The Yaml file path
     * @param bool $append If true, will append the new config to the existing config. If false will replace it
     * @return bool
     */
    public static function loadYaml($file, $append = true)
    {
        $yaml = new Parser();

        if (!file_exists($file)) {
            return false;
        }

        $newConfig = $yaml->parse(file_get_contents($file));

        if ($append) {
            self::$config = array_merge_recursive(self::$config, $newConfig);
        } else {
            self::$config = $newConfig;
        }

        return true;
    }

    /**
     * Load an array to application configuration
     *
     * @param array $array Associative array containing configuration values
     * @return bool
     */
    public static function loadArray($array)
    {
        self::$config = array_merge_recursive(self::$config, $array);
        return true;
    }

    /**
     * Key can be a keyword, that will return the base level object (ex: 'database')
     * or a 'dot' separated list of keywords (ex: memcache.prefix)
     *
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        $keys = explode('.', $key);

        $config = self::$config;
        foreach ($keys as $key) {

            if (!isset($config[$key])) {
                return false;
            }

            $config = $config[$key];
        }

        return $config;
    }

    /**
     * Reset the configuration array
     */
    public static function reset()
    {
        self::$config = array();
    }

    /**
     * Return the entire config array. This should only be used for debugging purposes.
     *
     * @return array
     */
    public static function getAll() {
        return self::$config;
    }

}