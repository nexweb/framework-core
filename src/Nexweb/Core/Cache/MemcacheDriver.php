<?php

namespace Nexweb\Core\Cache;

class MemcacheDriver implements DriverInterface
{

    /**
     * @var \Memcache[]
     */
    private $servers = array();

    /**
     * @var int
     */
    private $defaultTtl = 3600;

    /**
     * @var string
     */
    private $keyPrefix = "";

    /**
     * MemcacheDriver constructor.
     * @param string $cacheId
     * @param array $config
     */
    public function __construct($cacheId, $config)
    {

        if (!isset($config['servers']) || !is_array($config['servers'])) {
            throw new \RuntimeException('Invalid cache servers');
        }

        // Test for memcache extension
        if (!class_exists('Memcache', false)) {
            throw new \RuntimeException('Class Memcache not found');
        }

        if (isset($config['default_ttl'])) {
            $this->defaultTtl = $config['default_ttl'];
        }

        if (isset($config['key_prefix'])) {
            $this->keyPrefix = $config['key_prefix'];
        }

        if (isset($config['persistent'])) {
            if (!is_bool($config['persistent'])) {
                throw new \RuntimeException('Invalid persistent value (true, false)');
            }
            $persistent = $config['persistent'];
        } else {
            $persistent = true;
        }

        if (isset($config['retry_interval'])) {
            $retryInterval = $config['retry_interval'];
        } else {
            $retryInterval = 15;
        }

        $serverId = 0;
        foreach ($config['servers'] as $server) {
            $this->servers[$serverId] = new \Memcache();

            $serverWeight = isset($server['weight']) ? $server['weight'] : 1;
            $this->servers[$serverId]->addserver($server['host'], $server['port'], $persistent, $serverWeight, 1, $retryInterval);

            $serverId++;
        }
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        if (empty($key) || empty($this->servers)) {
            return false;
        }

        $key = $this->getKey($key);

        return $this->getServer($key)->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return bool
     */
    public function set($key, $value, $ttl = null)
    {
        if (empty($key) || empty($this->servers)) {
            return false;
        }

        if (is_null($ttl)) {
            $ttl = $this->defaultTtl;
        }

        $key = $this->getKey($key);

        return $this->getServer($key)->set($key, $value, MEMCACHE_COMPRESSED, $ttl);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete($key)
    {
        if (empty($key) || empty($this->servers)) {
            return false;
        }

        $key = $this->getKey($key);

        return $this->getServer($key)->delete($key, 0);
    }

    /**
     * Add an element into an array (does not allow duplicates)
     *
     * @param string $key
     * @param mixed $id
     * @return bool
     */
    public function insertId($key, $id)
    {

        if (empty($key) || empty($this->servers)) {
            return false;
        }

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        } elseif (!in_array($id, $cachedData)) {
            array_unshift($cachedData, $id);
            $this->set($key, $cachedData);
        }

        return true;
    }

    /**
     * Remove an element from an array
     *
     * @param string $key
     * @param mixed $id
     * @return bool
     */
    public function deleteId($key, $id)
    {

        if (empty($key) || empty($this->servers)) {
            return false;
        }

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        } else {
            $k = array_search($id, $cachedData);
            if($k !== false) {
                unset($cachedData[$k]);
            }
            $this->set($key, $cachedData);
        }

        return true;
    }

    /**
     * Change a property inside the object stored in cache
     *
     * @param string $key
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    public function updateValueInRow($key, $field, $value)
    {

        if (empty($key) || empty($this->servers)) {
            return false;
        }

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        } else {
            $cachedData->$field = $value;
            $this->set($key, $cachedData);
        }

        return true;
    }

    /**
     * Increment the value of a property in a stored object
     *
     * @param string $key
     * @param string $field
     * @param int $increment
     * @return bool
     */
    public function incrementValueInRow($key, $field, $increment = 1)
    {

        if (empty($key) || empty($this->servers)) {
            return false;
        }

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        } else {
            $cachedData->$field += $increment;
            $this->set($key, $cachedData);
        }

        return true;
    }

    /**
     * Decrement the value of a property in a stored object
     *
     * @param string $key
     * @param string $field
     * @param int $increment
     * @return bool
     */
    public function decrementValueInRow($key, $field, $increment = 1)
    {

        if (empty($key) || empty($this->servers)) {
            return false;
        }

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        } else {
            $cachedData->$field -= $increment;
            $this->set($key, $cachedData);
        }

        return true;
    }

    /**
     * Flush a specific server
     *
     * @param int $serverId
     * @return bool
     */
    public function flush($serverId)
    {
        if (!isset($this->servers[$serverId])) {
            return false;
        }

        $this->servers[$serverId]->flush();
        return true;
    }

    /**
     * Flush all servers
     */
    public function flushAll()
    {
        $success = false;
        foreach ($this->servers as $server) {
            $success = $success & $server->flush();
        }

        return $success;
    }

    /**
     * Get stats by memcache servers
     *
     * @return array
     */
    public function stats() {
        $stats = array();
        foreach ($this->servers as $i => $cache) {
            /** @noinspection PhpVoidFunctionResultUsedInspection */
            $stats[$i] = $cache->getstats();
        }

        return $stats;
    }

    /**
     * Data is spread other N memcache servers, this method takes a key and returns the corresponding server
     *
     * @param string $key
     * @return \Memcache
     */
    protected function getServer($key)
    {
        if (count($this->servers) == 1) {
            $serverId = 0;
        }
        else {
            $crc = sprintf("%u", crc32($key));
            $serverId = $crc % count($this->servers);
        }

        return $this->servers[$serverId];
    }

    /**
     * @param string $key
     * @return string
     */
    protected function getKey($key)
    {
        return 'Memcache|' . $this->keyPrefix . '|' . $key;
    }

    /**
     * @param array $keys
     * @return mixed
     */
    public function getMulti($keys)
    {
        // Not implemented in Memcache
        return false;
    }

    /**
     * Return the cached version version if available, otherwise execute the callback, store and return the result
     *
     * @param string $key
     * @param callable $callable
     * @param int|null $ttl
     * @return mixed
     */
    public function getOrSet($key, $callable, $ttl = null)
    {
        if (($value = $this->get($key)) !== false) {
            return $value;
        }

        $value = call_user_func($callable);
        $this->set($key, $value, $ttl);

        return $value;
    }
}