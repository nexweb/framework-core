<?php

namespace Nexweb\Core\Cache;

class NoCacheDriver implements DriverInterface
{

    public function __construct($cacheId, $config)
    {

    }

    public function get($key)
    {
        return false;
    }

    public function set($key, $value, $ttl = null)
    {
        return false;
    }

    public function delete($key)
    {
        return false;
    }

    public function insertId($key, $id)
    {
        return false;
    }

    public function deleteId($key, $id)
    {
        return false;
    }

    public function updateValueInRow($key, $field, $value)
    {
        return false;
    }

    public function incrementValueInRow($key, $field, $increment = 1)
    {
        return false;
    }

    public function decrementValueInRow($key, $field, $increment = 1)
    {
        return false;
    }

    public function flush($poolId)
    {
        return false;
    }

    public function flushAll()
    {
        return false;
    }

    public function stats()
    {
        return false;
    }

    public function getMulti($keys)
    {
        return false;
    }

    public function getOrSet($key, $callable, $ttl = null)
    {
        return call_user_func($callable);
    }
}
