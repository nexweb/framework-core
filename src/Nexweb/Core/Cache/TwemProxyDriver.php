<?php

namespace Nexweb\Core\Cache;

class TwemProxyDriver implements DriverInterface
{

    /**
     * @var \Memcached
     */
    private $instance;

    /**
     * @var int
     */
    private $defaultTtl = 3600;

    /**
     * @var string
     */
    private $keyPrefix = "";

    /**
     * MemcacheDriver constructor.
     * @param string $cacheId
     * @param array $config
     */
    public function __construct($cacheId, $config)
    {

        if (!isset($config['servers']) || !is_array($config['servers'])) {
            throw new \RuntimeException('Invalid cache servers');
        }

        // Test for memcache extension
        if (!class_exists('Memcached', false)) {
            throw new \RuntimeException('Class Memcached not found');
        }

        if (isset($config['default_ttl'])) {
            $this->defaultTtl = $config['default_ttl'];
        }

        if (isset($config['key_prefix'])) {
            $this->keyPrefix = $config['key_prefix'];
        }

        if (isset($config['persistent'])) {
            if (!is_bool($config['persistent'])) {
                throw new \RuntimeException('Invalid persistent value (true, false)');
            }
            $persistent = $config['persistent'];
        } else {
            $persistent = true;
        }

        $servers = $config['servers'];

        // Randomize the server list, and choose the first one that works
        shuffle($servers);

        foreach ($servers as $server) {
            $instance = $this->connect($server, $persistent);
            if ($this->isConnectionAlive($instance)) {
                $this->instance = $instance;
                break;
            }
        }

        if (empty($this->instance)) {
            throw new \RuntimeException('Connection to memcache failed');
        }
    }

    /**
     * @param \Memcached $instance
     * @return true
     */
    protected function isConnectionAlive($instance)
    {
        $instance->set('test_key', 'test_value');
        $resultMessage = $instance->getResultMessage();

        return ($resultMessage == 'SUCCESS' || $resultMessage == 'ACTION QUEUED');
    }

    protected function connect($server, $persistent)
    {
        if ($persistent) {
            $instance = new \Memcached($server['host'] . ':' . $server['port']);
        } else {
            $instance = new \Memcached();
        }

        $instance->setOption(\Memcached::OPT_PREFIX_KEY, 'Memcached|' . $this->keyPrefix . '|');

        $instance->setOption(\Memcached::OPT_COMPRESSION, true);
        $instance->setOption(\Memcached::OPT_COMPRESSION_TYPE, \Memcached::COMPRESSION_FASTLZ);

        $instance->setOption(\Memcached::OPT_BUFFER_WRITES, true);
        $instance->setOption(\Memcached::OPT_NO_BLOCK, true);
        $instance->setOption(\Memcached::OPT_TCP_NODELAY, true);
        $instance->setOption(\Memcached::OPT_BINARY_PROTOCOL, false); // unsupported

        if (!$persistent || count($instance->getServerList()) == 0) {
            $instance->addServer($server['host'], $server['port']);
        }

        return $instance;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        $key = $this->sanitizeKey($key);

        return $this->instance->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return bool
     */
    public function set($key, $value, $ttl = null)
    {
        $key = $this->sanitizeKey($key);

        if (is_null($ttl)) {
            $ttl = $this->defaultTtl;
        }

        return $this->instance->set($key, $value, $ttl);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete($key)
    {
        $key = $this->sanitizeKey($key);

        return $this->instance->delete($key, 0);
    }

    /**
     * Add an element into an array (does not allow duplicates)
     *
     * @param string $key
     * @param mixed $id
     * @return bool
     */
    public function insertId($key, $id)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        if (!in_array($id, $cachedData)) {
            array_unshift($cachedData, $id);
            return $this->set($key, $cachedData);
        }

        return true;
    }

    /**
     * Remove an element from an array
     *
     * @param string $key
     * @param mixed $id
     * @return bool
     */
    public function deleteId($key, $id)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        $k = array_search($id, $cachedData);
        if ($k !== false) {
            unset($cachedData[$k]);
        }

        return $this->set($key, $cachedData);
    }

    /**
     * Change a property inside the object stored in cache
     *
     * @param string $key
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    public function updateValueInRow($key, $field, $value)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        $cachedData->$field = $value;
        return $this->set($key, $cachedData);
    }

    /**
     * Increment the value of a property in a stored object
     *
     * @param string $key
     * @param string $field
     * @param int $increment
     * @return bool
     */
    public function incrementValueInRow($key, $field, $increment = 1)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        $cachedData->$field += $increment;
        return $this->set($key, $cachedData);
    }

    /**
     * Decrement the value of a property in a stored object
     *
     * @param string $key
     * @param string $field
     * @param int $increment
     * @return bool
     */
    public function decrementValueInRow($key, $field, $increment = 1)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        $cachedData->$field -= $increment;
        return $this->set($key, $cachedData);
    }

    /**
     * Flush a specific server
     *
     * @param int $serverId
     * @return bool
     */
    public function flush($serverId)
    {
        return $this->instance->flush();
    }

    /**
     * Flush all servers
     */
    public function flushAll()
    {
        return $this->instance->flush();
    }

    /**
     * Get stats
     *
     * @return array
     */
    public function stats()
    {
        return $this->instance->getStats();
    }

    /**
     * @param array $keys
     * @return mixed
     */
    public function getMulti($keys)
    {
        foreach ($keys as &$key) {
            $key = $this->sanitizeKey($key);
        }

        $null = null;
        return $this->instance->getMulti($keys, $null, \Memcached::GET_PRESERVE_ORDER);
    }

    /**
     * Sanitize key so memcached won't crash
     * @param string $key
     * @return string
     */
    protected function sanitizeKey($key)
    {
        return preg_replace('/\s+/', '_', $key);
    }

    /**
     * Return the cached version version if available, otherwise execute the callback, store and return the result
     *
     * @param string $key
     * @param callable $callable
     * @param int|null $ttl
     * @return mixed
     */
    public function getOrSet($key, $callable, $ttl = null)
    {
        $key = $this->sanitizeKey($key);

        $value = $this->get($key);

        if ($this->instance->getResultCode() != \Memcached::RES_NOTFOUND) {
            return $value;
        }

        $value = call_user_func($callable);
        $this->set($key, $value, $ttl);

        return $value;
    }
}
