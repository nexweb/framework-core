<?php

namespace Nexweb\Core\Cache;

interface DriverInterface
{
    /**
     * @param string $key
     * @return mixed
     */
    public function get($key);

    /**
     * @param array $keys
     * @return mixed
     */
    public function getMulti($keys);

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return bool
     */
    public function set($key, $value, $ttl = null);

    /**
     * @param string $key
     * @return bool
     */
    public function delete($key);

    /**
     * Add an element into an array (does not allow duplicates)
     *
     * @param string $key
     * @param mixed $id
     * @return bool
     */
    public function insertId($key, $id);

    /**
     * Remove an element from an array
     *
     * @param string $key
     * @param mixed $id
     * @return bool
     */
    public function deleteId($key, $id);

    /**
     * Change a property inside the object stored in cache
     *
     * @param string $key
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    public function updateValueInRow($key, $field, $value);

    /**
     * Increment the value of a property in a stored object
     *
     * @param string $key
     * @param string $field
     * @param int $increment
     * @return bool
     */
    public function incrementValueInRow($key, $field, $increment = 1);

    /**
     * Decrement the value of a property in a stored object
     *
     * @param string $key
     * @param string $field
     * @param int $increment
     * @return bool
     */
    public function decrementValueInRow($key, $field, $increment = 1);

    /**
     * Flush a specific pool
     *
     * @param int $poolId
     * @return bool
     */
    public function flush($poolId);

    /**
     * Flush all pools
     */
    public function flushAll();

    /**
     * Get stats by memcache pools
     *
     * @return array
     */
    public function stats();

    /**
     * Return the cached version version if available, otherwise execute the callback, store and return the result
     *
     * @param string $key
     * @param callable $callable
     * @param int|null $ttl
     * @return mixed
     */
    public function getOrSet($key, $callable, $ttl = null);
}
