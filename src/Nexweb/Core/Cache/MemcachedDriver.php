<?php

namespace Nexweb\Core\Cache;

class MemcachedDriver implements DriverInterface
{

    /**
     * @var \Memcached
     */
    private $instance;

    /**
     * @var int
     */
    private $defaultTtl = 3600;

    /**
     * @var string
     */
    private $keyPrefix = "";

    /**
     * MemcacheDriver constructor.
     * @param string $cacheId
     * @param array $config
     */
    public function __construct($cacheId, $config)
    {

        if (!isset($config['servers']) || !is_array($config['servers'])) {
            throw new \RuntimeException('Invalid cache servers');
        }

        // Test for memcache extension
        if (!class_exists('Memcached', false)) {
            throw new \RuntimeException('Class Memcached not found');
        }

        if (isset($config['default_ttl'])) {
            $this->defaultTtl = $config['default_ttl'];
        }

        if (isset($config['key_prefix'])) {
            $this->keyPrefix = $config['key_prefix'];
        }

        if (isset($config['persistent'])) {
            if (!is_bool($config['persistent'])) {
                throw new \RuntimeException('Invalid persistent value (true, false)');
            }
            $persistent = $config['persistent'];
        } else {
            $persistent = true;
        }

        if ($persistent) {
            $this->instance = new \Memcached($cacheId);
        } else {
            $this->instance = new \Memcached();
        }

        $this->instance->setOption(\Memcached::OPT_PREFIX_KEY, 'Memcached|' . $this->keyPrefix . '|');

        $this->instance->setOption(\Memcached::OPT_COMPRESSION, true);
        $this->instance->setOption(\Memcached::OPT_COMPRESSION_TYPE, \Memcached::COMPRESSION_FASTLZ);

        $this->instance->setOption(\Memcached::OPT_BUFFER_WRITES, true);
        $this->instance->setOption(\Memcached::OPT_NO_BLOCK, true);
        $this->instance->setOption(\Memcached::OPT_TCP_NODELAY, true);
        $this->instance->setOption(\Memcached::OPT_BINARY_PROTOCOL, false); // unsupported

        if (!$persistent || count($this->instance->getServerList()) == 0) {
            $this->instance->addServers($config['servers']);
        }
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        $key = $this->sanitizeKey($key);

        return $this->instance->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return bool
     */
    public function set($key, $value, $ttl = null)
    {
        $key = $this->sanitizeKey($key);

        if (is_null($ttl)) {
            $ttl = $this->defaultTtl;
        }

        return $this->instance->set($key, $value, $ttl);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete($key)
    {
        $key = $this->sanitizeKey($key);

        return $this->instance->delete($key, 0);
    }

    /**
     * Add an element into an array (does not allow duplicates)
     *
     * @param string $key
     * @param mixed $id
     * @return bool
     */
    public function insertId($key, $id)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        if (!in_array($id, $cachedData)) {
            array_unshift($cachedData, $id);
            return $this->set($key, $cachedData);
        }

        return true;
    }

    /**
     * Remove an element from an array
     *
     * @param string $key
     * @param mixed $id
     * @return bool
     */
    public function deleteId($key, $id)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        $k = array_search($id, $cachedData);
        if ($k !== false) {
            unset($cachedData[$k]);
        }

        return $this->set($key, $cachedData);
    }

    /**
     * Change a property inside the object stored in cache
     *
     * @param string $key
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    public function updateValueInRow($key, $field, $value)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        $cachedData->$field = $value;
        return $this->set($key, $cachedData);
    }

    /**
     * Increment the value of a property in a stored object
     *
     * @param string $key
     * @param string $field
     * @param int $increment
     * @return bool
     */
    public function incrementValueInRow($key, $field, $increment = 1)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        $cachedData->$field += $increment;
        return $this->set($key, $cachedData);
    }

    /**
     * Decrement the value of a property in a stored object
     *
     * @param string $key
     * @param string $field
     * @param int $increment
     * @return bool
     */
    public function decrementValueInRow($key, $field, $increment = 1)
    {
        $key = $this->sanitizeKey($key);

        $cachedData = $this->get($key);
        if ($cachedData === false) {
            return false;
        }

        $cachedData->$field -= $increment;
        return $this->set($key, $cachedData);
    }

    /**
     * Flush a specific server
     *
     * @param int $serverId
     * @return bool
     */
    public function flush($serverId)
    {
        return $this->instance->flush();
    }

    /**
     * Flush all servers
     */
    public function flushAll()
    {
        return $this->instance->flush();
    }

    /**
     * Get stats
     *
     * @return array
     */
    public function stats()
    {
        return $this->instance->getStats();
    }

    /**
     * @param array $keys
     * @return mixed
     */
    public function getMulti($keys)
    {
        foreach ($keys as &$key) {
            $key = $this->sanitizeKey($key);
        }

        // getMulti signature changed in memcached version 3+
        if (phpversion('memcached') >= 3.0) {
            return $this->instance->getMulti($keys, \Memcached::GET_PRESERVE_ORDER);
        } else {
            $null = null;
            return $this->instance->getMulti($keys, $null, \Memcached::GET_PRESERVE_ORDER);
        }
    }

    /**
     * Sanitize key so memcached won't crash
     * @param string $key
     * @return string
     */
    public function sanitizeKey($key)
    {
        $key = preg_replace('/[[:space:][:cntrl:]]/', '_', $key);

        // Get the real prefix
        $prefix = $this->instance->getOption(\Memcached::OPT_PREFIX_KEY);

        // Max key length in 250 chars
        if (strlen($prefix) + strlen($key) > 250) {
            $key = md5($key);
        }

        return $key;
    }

    /**
     * Return the cached version version if available, otherwise execute the callback, store and return the result
     *
     * @param string $key
     * @param callable $callable
     * @param int|null $ttl
     * @return mixed
     */
    public function getOrSet($key, $callable, $ttl = null)
    {
        $key = $this->sanitizeKey($key);

        $value = $this->get($key);

        if ($this->instance->getResultCode() != \Memcached::RES_NOTFOUND) {
            return $value;
        }

        $value = call_user_func($callable);
        $this->set($key, $value, $ttl);

        return $value;
    }
}
