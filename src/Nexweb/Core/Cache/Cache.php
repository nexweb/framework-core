<?php

namespace Nexweb\Core\Cache;

use Nexweb\Core\Config\Config;

class Cache
{

    public static $DRIVERS = [
        'memcache' => 'MemcacheDriver',
        'memcached' => 'MemcachedDriver',
        'twemproxy' => 'TwemProxyDriver'
    ];

    /**
     * @var DriverInterface[]
     */
    private static $cacheInstances = [];

    /**
     * @param string|null $cacheId
     * @return DriverInterface
     */
    public static function getInstance($cacheId = null)
    {

        // Grab memcache pools from the configuration file
        $configs = Config::get('memcache');
        if (empty($configs)) {
            if (empty(self::$cacheInstances[$cacheId])) {
                self::$cacheInstances[$cacheId] = new NoCacheDriver($cacheId, []);
            }
            return self::$cacheInstances[$cacheId];
        }

        // Default cache pool is the first one
        if (is_null($cacheId)) {
            $cacheIds = array_keys($configs);
            $cacheId = $cacheIds[0];
        }

        if (!isset($configs[$cacheId])) {
            throw new \RuntimeException('Cache ID ' . $cacheId . ' does not exist');
        }

        if (isset(self::$cacheInstances[$cacheId])) {
            return self::$cacheInstances[$cacheId];
        }

        $config = $configs[$cacheId];

        if (!isset($config['servers'])) {
            throw new \RuntimeException('No servers provided for ' . $cacheId);
        }

        // Default driver is 'memcache'
        $driver = isset($config['driver']) ? strtolower($config['driver']) : 'memcache';

        if (!array_key_exists($driver, self::$DRIVERS)) {
            throw new \RuntimeException('Unsupported driver "' . $driver . '"');
        }

        $class = __NAMESPACE__ . '\\' . self::$DRIVERS[$driver];

        self::$cacheInstances[$cacheId] = new $class($cacheId, $config);

        return self::$cacheInstances[$cacheId];
    }
}